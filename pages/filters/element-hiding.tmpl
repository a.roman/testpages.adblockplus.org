template = testcase
title = Element Hiding
description = Check that basic element hiding functionality is working as expected.

<section class="testcase-panel">
  {{ heading("ID Selector") }}
  <p>Test that an element hiding filter using an ID selector hides its target.</p>
  <div class="testcase-area">
    <div id="eh-id" data-expectedresult="fail">Target</div>
    <div class="testcase-examplecontent">Example Content</div>
  </div>
  <h3>Steps</h3>
  <ul class="testcase-steps">
    <li>Add filter #1.</li>
    <li>Refresh page.</li>
    <li>The red element should be hidden and the space collapsed.</li>
  </ul>
  <h3>Filters</h3>
  <ul class="testcase-filters">
    <li><pre>{{ site_url|domain }}###eh-id</pre></li>
  </ul>
</section>

<section class="testcase-panel">
  {{ heading("Class Selector") }}
  <p>Test that an element hiding filter using a class selector hides its target.</p>
  <div class="testcase-area">
    <div class="eh-class" data-expectedresult="fail">Target</div>
    <div class="testcase-examplecontent">Example Content</div>
  </div>
  <h3>Steps</h3>
  <ul class="testcase-steps">
    <li>Add filter #1.</li>
    <li>Refresh page.</li>
    <li>The red element should be hidden and the space collapsed.</li>
  </ul>
  <h3>Filters</h3>
  <ul class="testcase-filters">
    <li><pre>{{ site_url|domain }}##.eh-class</pre></li>
  </ul>
</section>

<section class="testcase-panel">
  {{ heading("Descendant Selector") }}
  <p>Test that an element hiding filter using a descendant selector hides its target.</p>
  <div class="testcase-area">
    <div class="eh-descendant" data-expectedresult="fail">Target</div>
    <div class="testcase-examplecontent">Example Content</div>
  </div>
  <h3>Steps</h3>
  <ul class="testcase-steps">
    <li>Add filter #1.</li>
    <li>Refresh page.</li>
    <li>The red element should be hidden and the space collapsed.</li>
  </ul>
  <h3>Filters</h3>
  <ul class="testcase-filters">
    <li><pre>{{ site_url|domain }}##.testcase-area > .eh-descendant</pre></li>
  </ul>
</section>

<section class="testcase-panel">
  {{ heading("Sibling Selector") }}
  <p>Test that an element hiding filter using a sibling selector hides its target.</p>
  <div class="testcase-area">
    <div class="testcase-examplecontent">Example Content</div>
    <div class="eh-sibling" data-expectedresult="fail">Target</div>
  </div>
  <h3>Steps</h3>
  <ul class="testcase-steps">
    <li>Add filter #1.</li>
    <li>Refresh page.</li>
    <li>The red element should be hidden and the space collapsed.</li>
  </ul>
  <h3>Filters</h3>
  <ul class="testcase-filters">
    <li><pre>{{ site_url|domain }}##.testcase-examplecontent + .eh-sibling</pre></li>
  </ul>
</section>

<section class="testcase-panel">
  {{ heading("Attribute Selector 1") }}
  <p>Test that an element hiding filter using an attribute selector hides its target.</p>
  <div class="testcase-area">
    <div width="100" height="100" class="eh-sibling" data-expectedresult="fail">Target</div>
    <div class="testcase-examplecontent">Example Content</div>
  </div>
  <h3>Steps</h3>
  <ul class="testcase-steps">
    <li>Add filter #1.</li>
    <li>Refresh page.</li>
    <li>The red element should be hidden and the space collapsed.</li>
  </ul>
  <h3>Filters</h3>
  <ul class="testcase-filters">
    <li><pre>{{ site_url|domain }}##div[height="100"][width="100"]</pre></li>
  </ul>
</section>

<section class="testcase-panel">
  {{ heading("Attribute Selector 2") }}
  <p>Test that an element hiding filter using an attribute selector hides its target.</p>
  <div class="testcase-area">
    <div href="http://testcase-attribute.com/" class="eh-sibling" data-expectedresult="fail">Target</div>
    <div class="testcase-examplecontent">Example Content</div>
  </div>
  <h3>Steps</h3>
  <ul class="testcase-steps">
    <li>Add filter #1.</li>
    <li>Refresh page.</li>
    <li>The red element should be hidden and the space collapsed.</li>
  </ul>
  <h3>Filters</h3>
  <ul class="testcase-filters">
    <li><pre>{{ site_url|domain }}##div[href="http://testcase-attribute.com/"]</pre></li>
  </ul>
</section>

<section class="testcase-panel">
  {{ heading("Attribute Selector 3") }}
  <p>Test that an element hiding filter using an attribute selector hides its target.</p>
  <div class="testcase-area">
    <div style="width: 200px;" class="eh-sibling" data-expectedresult="fail">Target</div>
    <div class="testcase-examplecontent">Example Content</div>
  </div>
  <h3>Steps</h3>
  <ul class="testcase-steps">
    <li>Add filter #1.</li>
    <li>Refresh page.</li>
    <li>The red element should be hidden and the space collapsed.</li>
  </ul>
  <h3>Filters</h3>
  <ul class="testcase-filters">
    <li><pre>{{ site_url|domain }}##div[style="width: 200px;"]</pre></li>
  </ul>
</section>

<section class="testcase-panel">
  {{ heading("Starts with Selector 1") }}
  <p>Test that an element hiding filter using a starts with selector hides its target.</p>
  <div class="testcase-area">
    <div href="http://testcase-startswith.com/test" class="eh-sibling" data-expectedresult="fail">Target</div>
    <div class="testcase-examplecontent">Example Content</div>
  </div>
  <h3>Steps</h3>
  <ul class="testcase-steps">
    <li>Add filter #1.</li>
    <li>Refresh page.</li>
    <li>The red element should be hidden and the space collapsed.</li>
  </ul>
  <h3>Filters</h3>
  <ul class="testcase-filters">
    <li><pre>{{ site_url|domain }}##div[href^="http://testcase-startswith.com/"]</pre></li>
  </ul>
</section>

<section class="testcase-panel">
  {{ heading("Starts with Selector 2") }}
  <p>Test that an element hiding filter using a starts with selector hides its target.</p>
  <div class="testcase-area">
    <div style="width: 201px; color: white;" class="eh-sibling" data-expectedresult="fail">Target</div>
    <div class="testcase-examplecontent">Example Content</div>
  </div>
  <h3>Steps</h3>
  <ul class="testcase-steps">
    <li>Add filter #1.</li>
    <li>Refresh page.</li>
    <li>The red element should be hidden and the space collapsed.</li>
  </ul>
  <h3>Filters</h3>
  <ul class="testcase-filters">
    <li><pre>{{ site_url|domain }}##div[style^="width: 201px;"]</pre></li>
  </ul>
</section>

<section class="testcase-panel">
  {{ heading("Ends with Selector") }}
  <p>Test that an element hiding filter using a ends with selector hides its target.</p>
  <div class="testcase-area">
    <div style="color: white; width: 202px;" class="eh-sibling" data-expectedresult="fail">Target</div>
    <div class="testcase-examplecontent">Example Content</div>
  </div>
  <h3>Steps</h3>
  <ul class="testcase-steps">
    <li>Add filter #1.</li>
    <li>Refresh page.</li>
    <li>The red element should be hidden and the space collapsed.</li>
  </ul>
  <h3>Filters</h3>
  <ul class="testcase-filters">
    <li><pre>{{ site_url|domain }}##div[style$="width: 202px;"]</pre></li>
  </ul>
</section>

<section class="testcase-panel">
  {{ heading("Contains") }}
  <p>Test that an element hiding filter using a contains selector hides its target.</p>
  <div class="testcase-area">
    <div style="color: white; width: 203px;" class="eh-sibling" data-expectedresult="fail">Target</div>
    <div class="testcase-examplecontent">Example Content</div>
  </div>
  <h3>Steps</h3>
  <ul class="testcase-steps">
    <li>Add filter #1.</li>
    <li>Refresh page.</li>
    <li>The red element should be hidden and the space collapsed.</li>
  </ul>
  <h3>Filters</h3>
  <ul class="testcase-filters">
    <li><pre>{{ site_url|domain }}##div[style*="width: 203px;"]</pre></li>
  </ul>
</section>
